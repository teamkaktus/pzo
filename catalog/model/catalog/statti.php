<?php

class ModelCatalogStatti extends Model { 

	public function updateViewed($statti_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "statti SET viewed = (viewed + 1) WHERE statti_id = '" . (int)$statti_id . "'");
	}

	public function getStattiStory($statti_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "statti n LEFT JOIN " . DB_PREFIX . "statti_description nd ON (n.statti_id = nd.statti_id) LEFT JOIN " . DB_PREFIX . "statti_to_store n2s ON (n.statti_id = n2s.statti_id) WHERE n.statti_id = '" . (int)$statti_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");
	
		return $query->row;
	}

	public function getStatti($data) {
				$sql = "SELECT * FROM " . DB_PREFIX . "statti n LEFT JOIN " . DB_PREFIX . "statti_description nd ON (n.statti_id = nd.statti_id) LEFT JOIN " . DB_PREFIX . "statti_to_store n2s ON (n.statti_id = n2s.statti_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'";

				$sort_data = array(
					'nd.title',
					'n.date_added'
				);

				if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
					$sql .= " ORDER BY " . $data['sort'];
				} else {
					$sql .= " ORDER BY nd.title";
				}

				if (isset($data['order']) && ($data['order'] == 'DESC')) {
					$sql .= " DESC";
				} else {
					$sql .= " ASC";
				}
	
				if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
				$data['start'] = 0;
				}		
				if ($data['limit'] < 1) {
				$data['limit'] = 10;
				}	
		
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				}	
		
				$query = $this->db->query($sql);
	
				return $query->rows;
				}

	public function getStattiShorts($limit) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "statti n LEFT JOIN " . DB_PREFIX . "statti_description nd ON (n.statti_id = nd.statti_id) LEFT JOIN " . DB_PREFIX . "statti_to_store n2s ON (n.statti_id = n2s.statti_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1' ORDER BY n.date_added DESC LIMIT " . (int)$limit); 
	
		return $query->rows;
	}

	public function getTotalStatti() {
     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "statti n LEFT JOIN " . DB_PREFIX . "statti_to_store n2s ON (n.statti_id = n2s.statti_id) WHERE n2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND n.status = '1'");
	
		if ($query->row) {
			return $query->row['total'];
		} else {
			return FALSE;
		}
	}	
}
?>
