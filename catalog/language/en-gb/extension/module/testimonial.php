<?php
// Text
$_['heading_title']	           = 'Отзывы';

$_['text_write']               = 'Написать отзыв';
$_['text_login']               = 'Please <a href="%s">login</a> or <a href="%s">register</a> to review';
$_['text_no_reviews']          = 'Нет отзывов.';
$_['text_note']                = '<span class="text-danger">Примечание:</span> HTML не переводится!';
$_['text_success']             = 'Спасибо за ваш отзыв. Он был отправлен веб-мастеру для одобрения.';

$_['text_mail_subject']        = 'You have a new testimonial (%s).';
$_['text_mail_waiting']	       = 'You have a new testimonial waiting.';
$_['text_mail_author']	       = 'Автор: %s';
$_['text_mail_rating']	       = 'Рейтинг: %s';
$_['text_mail_text']	       = 'Текст:';

// Entry
$_['entry_name']               = 'Ваше имя';
$_['entry_review']             = 'Ваш отзыв';
$_['entry_rating']             = 'Рейтинг';
$_['entry_good']               = 'Хорошо';
$_['entry_bad']                = 'Плохо';

// Button
$_['button_continue']          = 'Написать отзыв';

// Error
$_['error_name']               = 'Внимание: имя отзыва должно быть от 3 до 25 символов!';
$_['error_text']               = 'Внимание: текст проверки должен быть от 25 до 3000 символов!';
$_['error_rating']             = 'Внимание: выберите рейтинг обзора!';
$_['error_captcha']            = 'Предупреждение: код подтверждения не соответствует изображению!';

