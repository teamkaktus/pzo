<?php echo $header; ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class=" body_info">
        <div class="col-xs-12">
      <p class='title-head-body'><?php echo $heading_title; ?></p>
      <?php echo $description; ?>
    </div>

    <div class="information-class-my col-xs-12">
      <div class="click-all-subscribe">
        <div>
          <a class="subscribe-add-button" data-toggle="modal" data-target="#myModalpop">Оформить подписку</a>
          <div class="modal fade" id="myModalpop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-body" style="text-align: center; ">
                  <!--<div style="text-align: right;">
                      <button type="button" class="myClose" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>-->
                  <div class='my-class-text-right'>
                    <div class='exit-img-class' data-dismiss="modal" aria-label="Close">
                      <img src="/catalog/view/theme/default/img/exit_button.png">
                    </div>
                  </div>
                  <p class="modal-title">ОФОРМИТЬ ПОДПИСКУ</p>

	                  <div class="one-modal-input same-heght-width">
                    <input type="email" name="txtemail" id="txtemail" placeholder="E-mail">
                  </div>
                  <div class="two-modal-input same-heght-width">
                    <input type="text" name="txtfio" id="txtfio" placeholder="ФИО">
                  </div>
                  <div class="three-modal-input same-heght-width">
                    <input type="text" name="txtphone" id="txtphone" placeholder="Телефон">
                  </div>
                  <div>
                    <a class="myButtonPersonal" onclick="return subscribe();">Отправить</a>
                  </div>
                  <div class='info-modal-style'>
                    <p>Оформляя данную подписку, я даю согласие на обработку моих персональных данных</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
       
          </div>
        <div style="margin-top: 18px;">
          <a class="list-two-button-subscribe" href="index.php?route=product/category&path=65">Раздел «Огнезащитные материалы»</a>
        </div>

      </div>
      <div class="name-information">
        <p>Спасибо за внимание!</p>
        <p>Генеральный директор ООО «Первый Завод Огнезащиты»</p>
        <p>Кулик Иван Владимирович</p>
      </div>
    </div>
      <?php echo $content_bottom; ?></div>
   </div> <?php echo $column_right; ?>
</div>
<?php echo $footer; ?>