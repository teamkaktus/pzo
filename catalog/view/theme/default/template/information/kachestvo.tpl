<?php echo $header; ?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class=" body_info">
        <div class="col-xs-12">
      <p class='title-head-body'>Сертификация</p>
    </div>
           <div class="col-xs-12 no-padding">
                                <div class="col-sm-12 seo style-text-product">
                                    <div class="col-xs-12 no-padding">
					<h4><?php echo $heading_titleseo ?></h4>
					<?php echo $descriptionseo; ?>
                                    </div>
                                    
                                
                                </div>
                            </div>
   <div class="col-xs-12 no-padding">
                        <?php  for ($i=1; $i <= 12; $i++){ 
                            $pdfname = 'pdf'.$i; 
                            if (array_key_exists($pdfname, $pdf)) {           			
                                $size = filesize(DIR_IMAGE.$pdf[$pdfname]);
                                $size = $size/1000000;
                                $parts = explode("/", $pdf[$pdfname]);
                            ?>
                                <div class="pdf pdf_kachestvo col-xs-12 no-padding col-sm-6 no-padding" >
                                    <a target="_blank" href="<?php echo 'image/'.$pdf[$pdfname]; ?>">
                                        <div class="col-xs-1 col-sm-2 col-md-1 col-lg-1 img_img_pdf_a_href">
                                            <img src="../image/img_pdf.png" alt="">
                                        </div>
                                        <div class="col-xs-10 col-sm-10 col-md-11 col-lg-11 pdf_img_text_a_href">
                                            <?php echo  preg_replace("/(.pdf)(.*)/","", end($parts)); ?> (<?php echo round($size, 2); ?> мб)
                                        </div>
                                    </a>
                                </div><?php
                            }
                        } ?>
                    </div>
			   
    <div class="information-class-my col-xs-12">
      <div class="click-all-subscribe">
        <div>
          <a class="subscribe-add-button" data-toggle="modal" data-target="#myModal">Оформить подписку</a>
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-body" style="text-align: center; ">
                  <!--<div style="text-align: right;">
                      <button type="button" class="myClose" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>-->
                  <div class='my-class-text-right'>
                    <div class='exit-img-class' data-dismiss="modal" aria-label="Close">
                      <img src="/catalog/view/theme/default/img/exit_button.png">
                    </div>
                  </div>
                  <p class="modal-title">ОФОРМИТЬ ПОДПИСКУ</p>
                  <div class="one-modal-input same-heght-width">
                    <input type="email" placeholder="E-mail">
                  </div>
                  <div class="two-modal-input same-heght-width">
                    <input type="text" placeholder="ФИО">
                  </div>
                  <div class="three-modal-input same-heght-width">
                    <input type="text" placeholder="Телефон">
                  </div>
                  <div>
                    <a class="myButtonPersonal">Отправить</a>
                  </div>
                  <div class='info-modal-style'>
                    <p>Оформляя данную подписку, я даю согласие на обработку моих персональных данных</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div style="margin-top: 18px;">
          <a class="list-two-button-subscribe" href="index.php?route=product/category&path=65">Раздел «Огнезащитные материалы»</a>
        </div>
    
      </div>
      <div class="name-information">
        <p>Спасибо за внимание!</p>
        <p>Генеральный директор ООО «Первый Завод Огнезащиты»</p>
        <p>Кулик Иван Владимирович</p>
      </div>
    </div>
     </div> <?php echo $content_bottom; ?>
    </div><?php echo $column_right; ?>
</div>
<?php echo $footer; ?>