</div>
<div style="border-bottom: 1px solid #ddd; margin-bottom: 34px">
</div>
<footer>
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 allfooter_mob">
            <div class="col-xs-12 col-sm-7" style="padding: 0px">
                <?php echo $search;?>

                <div class="col-xs-12 col-md-4 no_padding row1" style="padding-right: 0px;">
                    <p class="title_table"><a href="">Главная</a></p>

                    <p class="title_table row1"><a href="/index.php?route=information/information&information_id=4">О компании</a></p>

                    <ul class="text_table row1" style="padding: 0px; list-style-type: none; ">
                      <li><a href="/index.php?route=information/information&information_id=6">Миссия компании</a></li>
                      <li><a href="/index.php?route=information/information&information_id=3">Производство</a></li>
                      <li><a href="/index.php?route=information/information&information_id=5">Наша команда</a></li>
                      <li><a href="/index.php?route=information/information&information_id=7">Гарантии качества</a></li>
                      <li><a href="/index.php?route=information/information&information_id=8">Обращение к клиентам</a></li>
                      <li><a href="/index.php?route=information/information&information_id=9">Вакансии</a></li>
                    </ul>
                </div>

                <div class="col-xs-12 col-md-6 no_padding row1" style="padding-right: 0px;">
                    <p class="title_table "><a href="">Сертификация</a></p>
                    <p class="title_table"><a href="/index.php?route=product/category&path=65">Огнезащитные материалы</a></p>
                    
                    <ul class="text_table row1" style="padding: 0px; list-style-type: none;">
                        <?php  foreach ($categories['0']['children'] as $category) {  ?>
                        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="col-xs-12 col-md-2 no_padding row1 " style="padding-right: 0px">
                    <p class="title_table"><a href="/index.php?route=information/information&information_id=10">Законодательство</a></p>
                    <p class="title_table"><a href="/index.php?route=information/information&information_id=11">Дилерам</a></p>
                    <p class="title_table"><a href="/index.php?route=information/information&information_id=12">Поставщикам</a></p>
                    <p class="title_table"><a href="/index.php?route=testimonial/testimonial">Отзывы</a></p>
                    <p class="title_table"><a href="/index.php?route=information/information&information_id=13">Контакты</a></p>
                </div>
            </div>

            <div class=" menu_mobile_footer col-xs-12 col-sm-5  col-md-3" style="background-color: #f24941; height: 459px; ">
                <p class="title_right_foter">Свяжитесь с нами</p>
                <p class="text_right_foter">Текст текст текст текст текст текст текст текст текст текст текст текст
                    текст
                    текст текст</p>

                <form id='form_send_footer'> <!--action="/index.php?route=information/contact/send_index_message_form"-->
                    <p class="text_right_foter">Имя</p>
                    <input type="text" name="name" class="foter_right_menu name-correct-footer" placeholder="Имя" minlength="3" maxlength="32" required />
                    <p class="text_right_foter" style="padding-top: 16px;">Телефон</p>
                    <input type="text" name="phone" class="foter_right_menu phone-correct-footer" placeholder="+77777777777" required />

                    <button id='button_send_form' class="button_foter" type="submit">ПЕРЕЗВОНИТЕ МНЕ
                      <img style="margin-bottom: 3px; margin-left: 9px;" src="/catalog/view/theme/default/img/row_right.png">
                    </button>
                </form>
            </div>

            <div class="foter_log" style="background-image: url(<?php echo $logo; ?>);">
                <p>ЗАО "Технориск", 440008,</p>
                <p><?php echo $adress; ?></p>
                <p style="margin-bottom: 0px"> Тел. <?php echo $telephone; ?></p>
                <p style="margin-left: 259px"><?php echo $telephone1; ?></p>
            </div>
        </div>

        <div class="foter_log1" >
            <a href="<?php echo $home; ?>">
              <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" style="padding-top: 31px" />
            </a>
             
            
            <p style="padding-top: 15px;">ЗАО "Технориск", 440008,</p>
            <p><?php echo $adress; ?></p>
                <p style="margin-bottom: 0px"> Тел. <?php echo $telephone; ?></p>
                <p style="margin-left: 259px"><?php echo $telephone1; ?></p>
        </div>
        <div class="modal fade" id="myModalz"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body" style="text-align: center; ">
                        <div class='my-class-text-right'>
                            <div class='exit-img-class' data-dismiss="modal" aria-label="Close">
                                <img src="/catalog/view/theme/default/img/exit_button.png">
                            </div>
                        </div>
                        <p class="modal-title">ЗАКАЗАТЬ</p>
                        <div class="two-modal-input same-heght-width">
                            <input type="text" name="namez" id="namez" placeholder="ФИО">
                            <input type="text" name="nameprod" id="nameprod" class="hidden" value="f">
                        </div>
                        <div class="three-modal-input same-heght-width">
                            <input type="text" name="orgz" id="orgz" placeholder="Организация">
                        </div> 
                        <div class="three-modal-input same-heght-width">
                            <input type="text" name="mailz" id="mailz" placeholder="имеил">
                        </div> 
                        <div class="three-modal-input same-heght-width">
                            <input type="text" name="phonez" id="phonez" placeholder="Телефон">
                        </div>
                        <div>
                            <a class="myButtonPersonal" onclick="return subscribez();">Отправить</a>
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>    
       
    </div>
</footer>

</body>
</html>