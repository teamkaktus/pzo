<div id="search" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 input_padding" style="padding-bottom: 20px;">
  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="input_foter" placeholder="Поиск по сайту">
  <span class="input-group-btn span-btn-search">
    <button type="button" class="search btn btn-default btn-lg btn-my-xs"><i class="fa fa-search"></i></button>
  </span>
</div>