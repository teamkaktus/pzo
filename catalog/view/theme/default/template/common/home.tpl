<?php echo $header; ?><?php echo $content_top; ?>
<div class="container">
  <div class="row">
      
      <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">

  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style=" padding: 0px;">

      <div class=" body_info">
      <?= $a_description; ?>
      </div>

      <div class="mobile_center" style="padding: 0px;">
        <div class="" style="margin-top: 83px;">
          <p class="text_product" style="margin-top: 71px" data-toggle="collapse" data-target="#menu_text">
            <?= $і_heading_title; ?></p>
          <button class="button_about" data-toggle="collapse" data-target="#menu_text">ПОДРОБНЕЕ
            <img style="margin-bottom: 4px;" src="img/row_right.png">
          </button>
        </div>

        <div id="menu_text" class="collapse text_menu_style  " style="padding: 0px">
          <?= $і_description; ?>
        </div>

        <div class="col-xs-12 col-md-6" style="padding: 0px;">
          <div class="col-xs-12  title_stat" style="margin-top: 130px  ; border-bottom: 1px solid #ddd; padding-bottom: 35px;">
            <a href="/index.php?route=information/statti" style="color: #f24941;">НАШИ СТАТЬИ</a>

          </div>
          <?php foreach ($statti as $news_item) { ?>
          <div class="col-xs-12  title_stat" style="padding-top: 50px;">
            <p class="title_table"><?php echo $news_item['title']; ?></p>
            <p class="text_table"><?php echo $news_item['description']; ?></p>

            <p class="data_table"><?php echo $news_item['posted']; ?></p>
          </div>
          <?php } ?>







        </div>
        <div class="col-xs-12 col-md-6" style="padding: 0px;">
          <div class="col-xs-12 title_stat line_style novosty_company" style="margin-top: 130px ;border-bottom: 1px solid #ddd; padding-bottom: 35px; ">
            <a href="/index.php?route=information/news" style="color: #f24941;">НОВОСТИ КОМПАНИИ</a>
          </div>
          <?php foreach ($news as $news_item) { ?>
          <div class="col-xs-12  title_stat" style="padding-top: 50px;">
            <p class="title_table"><?php echo $news_item['title']; ?></p>
            <p class="text_table"><?php echo $news_item['description']; ?></p>

            <p class="data_table"><?php echo $news_item['posted']; ?></p>
          </div>
          <?php } ?>


        </div>
      </div>

  </div>

  </div> <?php echo $column_right; ?>
  </div><?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>