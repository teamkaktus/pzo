<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=500, initial-scale=0.5">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>

<link rel="stylesheet" type="text/css" href="catalog/view/javascript/OwlCarousel2-2.2.1/dist/assets/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/OwlCarousel2-2.2.1/dist/assets/owl.theme.default.css">
<script src="catalog/view/javascript/OwlCarousel2-2.2.1/dist/owl.carousel.js"></script>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/index_css.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/dima_fix.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="../catalog/view/javascript/js_site.js" type="text/javascript"></script>
<script src="../catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>"style="overflow-x: hidden;">
<div class="hedden">
  <div class="container hidden2">
    <div class="header">
      <div class="col-xs-12 col-sm-3 logo_in_header" >
        <a href="<?php echo $home; ?>">
          <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />
        </a>
      </div>
      <div class="col-xs-12 col-sm-9 col-md-9" style="padding-top: 28px">
        <div class=" col-sm-6 col-md-5 " style="padding: 0px">
            <div type="text" class="input_header_mob " style="position: relative"><p><a href="tel: <?php echo $telephone; ?>"><?php echo $telephone; ?></a></p></div>
          <div type="text" class="input_call_header"></div>
        </div>
        <div type="text" class="input_header_login col-xs-5 col-sm-5 col-md-7 "
             style="position: relative; padding: 0px">
            <p class="header_line" ><a href="mailto: <?php echo $email; ?>"><?php echo $email; ?></a></p>
        </div>
      </div>
    </div>
  </div>
  <div class=" " style="z-index:2; border-top: 1px solid #f24941; position: relative;"></div>
  <div class="container hidden2">
    <div class="" style="z-index: 1;">
      <nav class="navbar navbar-default" style="background-color: white; border: 0px; margin-bottom: 0px;">
        <div class="container-fluid">
          <div class="navbar-header ">
            <button type="button" class="navbar-toggle collapsed" style="background-color: #f24941;"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar" style="background-color: white"></span>
              <span class="icon-bar" style="background-color: white"></span>
              <span class="icon-bar" style="background-color: white"></span>
            </button>

          </div>
          <div id="navbar" class="navbar-collapse collapse menue_golovne" aria-expanded="false"
               style="height: 1px;">
            <ul class="nav navbar-nav menue_golovne">
              <li><a class="text " href="<?php echo $home; ?>">Главная</a></li>

              <li><a class="text" href="/index.php?route=information/information&information_id=4">O компании</a></li>
              <li><a class="text drop-down-hover" href="/index.php?route=product/category&path=65">Огнезащитные материалы</a>
                    <div class="dropdown-menu" style="background-color: rgba(52, 52, 52, 0.81);">
                             <div class="dropdown-inner">
                            <ul class="list-unstyled" style="padding: 10px;">
                                <?php foreach ($categories as $child) { ?>
                                <li><a href="<?php echo $child['href']; ?>" style="color: #ffffff;"><?php echo $child['name']; ?></a></li>
                                <?php } ?>
                              </ul>
                          </div>  
                    </div>
                </li>
              <li><a class="text" href="index.php?route=information/kachestvo">Сертификация</a></li>
              <li><a class="text" href="/index.php?route=information/information&information_id=10">Законодательство</a></li>
              <li><a class="text" href="/index.php?route=information/information&information_id=11">Дилерам</a></li>
              <li><a class="text" href="/index.php?route=testimonial/testimonial">Отзывы</a></li>
              <li><a class="text" href="/index.php?route=information/information&information_id=13">Контакты</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    </div>
  </div>
</div>
