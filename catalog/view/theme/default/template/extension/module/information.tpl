<div class="sidebar">
   <div class="col-xs-12 col-sm-12 col-md-12">
      <h1 style="font-size: 16px; color: #f24941; float: left; padding-left: 31px;  font-family: ProximaNova; padding-bottom: 16px; padding-top: 11px">
        ПОДБОР КРАСКИ</h1>
      <div class="col-xs-12 body_info_right">
        <p class="title_texy_button_right">Продукт будет применяться:</p>
        <button class="in_over">Внутри</button>
        <button class="in_over">Снаружи</button>

        <p class="title_texy_button_right">Тип поверхности:</p>
        <button class="in_over">Дерево</button>
        <button class="in_over">Камень</button>
        <button class="in_over">Металл</button>

        <p class="title_texy_button_right" style="color: #414141">Необходимое действие:</p>
        <select class="new-select-style-wpandyou" style="width:100%; height: 25px;  padding-top: 3px; padding-left: 10px; font-family: ProximaNova ;color: #8e8e8e;">
          <option>Нанести</option>
          <option>Нанести</option>
          <option>Нанести</option>

        </select>

        <button class="button_give">Подобрать
          <img src="img/row_right.png">
        </button>

        <p class="title_texy_button_right" style="margin-bottom: 2px;">Для решения ваших задач подойдут:</p>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding: 0px 22px 0px 5px;">
          <div class="col-xs-4">
            <img style="width: 84px;" src="img/kanistra.jpg">
          </div>
          <div class="col-xs-4">
            <img style="width: 84px;" src="img/kanistra.jpg">
          </div>
          <div class="col-xs-4">
            <img style="width: 84px;" src="img/kanistra.jpg">
          </div>
        </div>
        <p class="watch_resolt">Смотреть все результаты (12 шт)</p>
      </div>
      <div class=" " style=" bottom: 42px; position: relative;">
            
        <h1 class="hidden" style="font-size: 16px;color: #f24941;float: left;padding-left: 31px;font-family: ProximaNova;padding-bottom: 16px; padding-top: 55px;">
          РАСЧЕТ КРАСКИ</h1>
      </div>
      <div class="hidden" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 body_info_right" style="float: right; bottom: 36px; position: relative">

        <p class="title_texy_button_right">Выберите состав:</p>
        <select class="new-select-style-wpandyou" style="width:100%; height: 25px; padding-right: 29px; font-size: 12px;  padding-top: 3px; padding-left: 10px; font-family: ProximaNova ;color: #8e8e8e;">"&gt;

          <option>Антисептик для наружных и внутренних работ</option>
          <option>Антисептик для наружных и внутренних работ</option>
          <option>Антисептик для наружных и внутренних работ</option>

        </select>
        <p class="title_texy_button_right" style="margin: 0px">Укажите площадь обработки:</p>
        <input class="in_over" type="text" placeholder="50" ;="" style="width:35px; text-align: center; padding: 3px 0px 1px;">


        <p class="title_texy_button_right" style="display: inline-block">м<sup>2</sup></p>


        <p class="title_texy_button_right">Результат расчета:</p>

        <ul class="ul_decoration">
          <li>Площадь обработки: <b>50 <sup>2</sup></b></li>
          <li>Расход материала: <b>от 250 до 350г/м<sup>2</sup></b></li>
          <li>Количество готового раствора:<b> от 12.5 до 17.5 л</b></li>
          <li>Количество концентрата:<b> от 1.25 до 1.75 л</b></li>
        </ul>


        <p class="title_texy_button_right" style="margin-bottom: 2px;">Варианты фасовки продукта:</p>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text_style_img" style="padding: 0px 22px 20px 17px;">
          <div class="col-xs-4" style="padding: 0px">
            <img style="width: 84px;" src="img/kanistra.jpg">
            <p>30 л, 5 л, 1 л</p>
          </div>
          <div class="col-xs-4" style="padding: 0px">
            <img style="width: 84px;" src="img/kanistra.jpg">
            <p>30 л, 5 л, 1 л</p>
          </div>
          <div class="col-xs-4" style="padding: 0px">
            <img style="width: 84px;" src="img/kanistra.jpg">
            <p>30 л, 5 л, 1 л</p>
          </div>
        </div>

      </div>


    </div>

</div>