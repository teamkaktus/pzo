            <div class="slider hidden-xs hidden-sm">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <?php $i=0; foreach ($banners as $banner) { ?>
                        <div class="item <?php if($i==0) { echo 'active';} ?>">

                            <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />

                        </div>
                        <?php $i++; } ?>
                    </div>
                </div>
            </div>
