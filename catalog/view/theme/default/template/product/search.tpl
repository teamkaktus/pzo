<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb my-breadcrumb-style">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <label class="control-label" for="input-search"><?php echo $entry_search; ?></label>
      <div class="row">
        <div class="col-sm-4">
          <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />
        </div>
        <div class="col-sm-3">
          <select name="category_id" class="form-control">
            <option value="0"><?php echo $text_category; ?></option>
            <?php foreach ($categories as $category_1) { ?>
            <?php if ($category_1['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_1['children'] as $category_2) { ?>
            <?php if ($category_2['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_2['children'] as $category_3) { ?>
            <?php if ($category_3['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <div class="col-sm-3">
          <label class="checkbox-inline">
            <?php if ($sub_category) { ?>
            <input type="checkbox" name="sub_category" value="1" checked="checked" />
            <?php } else { ?>
            <input type="checkbox" name="sub_category" value="1" />
            <?php } ?>
            <?php echo $text_sub_category; ?></label>
        </div>
      </div>
      <p>
        <label class="checkbox-inline">
          <?php if ($description) { ?>
          <input type="checkbox" name="description" value="1" id="description" checked="checked" />
          <?php } else { ?>
          <input type="checkbox" name="description" value="1" id="description" />
          <?php } ?>
          <?php echo $entry_description; ?></label>
      </p>
      <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="button_about btn btn-primary" />
      <h2><?php echo $text_search; ?></h2>
      <?php if ($products) { ?>
      <div class="row">
        <?php foreach ($products as $product) { ?>

        <div class="col-xs-12 for-padding-style-my">
          <div class='col-sm-5 col-md-5 col-lg-4 image-title-cost'>
            <img src='<?php echo $product['thumb']; ?>'/>
          </div>
          <div class="col-sm-7 col-md-7 col-lg-8">
            <a href='<?php echo $product['href'];?>'>
            <p class="mytitle_table"><?php echo $product['name'];    ?></p></a>
            <?php if ($product['attribute_groups']) { ?>
            <div class='my-image-table col-xs-12'>
              <?php foreach ($product['attribute_groups'] as $attribute_group) { ?>
              <?php if ($attribute_group['attribute_group_id']==10) { ?>
              <div class='col-xs-12 col-sm-12 for-image-my no-padd-for-my'>
                <div class='col-xs-2 col-sm-1 col-md-1 col-lg-1' style='padding: 0;'>
                  <img src="/catalog/view/theme/default/img/m1.png"/>
                </div>
                <div class='col-xs-5 col-sm-5 col-md-6 col-lg-6 padd-left-none text-color-style-my-one'>
                  <span>Фасовка</span>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-6 col-lg-5 no-padding text-color-style-my-two">
                  <span class="text-size-my">- <?php  echo $attribute_group['attribute']['0']['text']; ?></span>
                </div>
              </div>
              <?php } ?>
              <?php if ($attribute_group['attribute_group_id']==9) { ?>
              <div class='col-xs-12 col-sm-12 for-image-my no-padd-for-my'>
                <div class='col-xs-2 col-sm-1 col-md-1 col-lg-1' style='padding: 0;'>
                  <img src="/catalog/view/theme/default/img/m1.png"/>
                </div>
                <div class='col-xs-5 col-sm-5 col-md-6 col-lg-6 padd-left-none text-color-style-my-one'>
                  <span>Огнезащитная ефективность</span>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-6 col-lg-5  no-padding text-color-style-my-two">
                  <span class="text-size-my">- <?php  echo $attribute_group['attribute']['0']['text']; ?></span>
                </div>
              </div>
              <?php } ?>
              <?php if ($attribute_group['attribute_group_id']==8) { ?>
              <div class='col-xs-12 col-sm-12 for-image-my no-padd-for-my'>
                <div class='col-xs-2 col-sm-1 col-md-1 col-lg-1' style='padding: 0;'>
                  <img src="/catalog/view/theme/default/img/m1.png"/>
                </div>
                <div class='col-xs-5 col-sm-5 col-md-6 col-lg-6 padd-left-none text-color-style-my-one'>
                  <span>Срок служби</span>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-6 col-lg-5  no-padding text-color-style-my-two">
                  <span class="text-size-my">- <?php  echo $attribute_group['attribute']['0']['text']; ?></span>
                </div>
              </div>
              <?php } ?>
              <?php if ($attribute_group['attribute_group_id']==7) {        ?>
              <div class='col-xs-12 col-sm-12 for-image-my no-padd-for-my'>
                <div class='col-xs-2 col-sm-1 col-md-1 col-lg-1' style='padding: 0;'>
                  <img src="/catalog/view/theme/default/img/m1.png"/>
                </div>
                <div class='col-xs-5 col-sm-5 col-md-6 col-lg-6 padd-left-none text-color-style-my-one'>
                  <span>Область применения</span>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-6 col-lg-5  no-padding text-color-style-my-two">
                  <span class="text-size-my">- <?php echo $attribute_group['attribute']['0']['name'];  ?>/<?php echo $attribute_group['attribute']['1']['name'];  ?></span>
                </div>
              </div>
              <?php } ?>
              <?php } ?>




            </div>
            <?php } ?>
            <div class="my-button-table col-xs-12">
              <button data-toggle="modal" data-target="#myModalz" class="button_about my-button-about" data-toggle="collapse"  onclick="f('<?php echo $product['name'];    ?>')">ЗАКАЗАТЬ
                <img style="height:12px; margin-bottom: 4px; margin-left: 10px;" src="/catalog/view/theme/default/img/row_right.png"/>
              </button>
            </div>
          </div>
        </div>

        <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
     </div>
    <?php echo $column_right; ?></div> <?php echo $content_bottom; ?>
</div>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>