<?php echo $header; ?>
<div class="container">
    <?php echo $content_top; ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">



      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -1px">
           <h4 style="font-size: 22px; font-family: 'ProximaNovaSemiBold';  color: rgb(0, 0, 0); text-transform: uppercase;  line-height: 1.2;     font-weight: 700;">КАТАЛОГ</h4>
        <div class="col-xs-12">
          <div class="my-class-product col-xs-12">
            <p style="    font-weight: 700; font-size: 25px; font-family: 'ProximaNovaSemiBold'; color: rgb(65, 65, 65); line-height: 1.2; "><?php echo $heading_title; ?></p>
            <div class="col-xs-12" style='padding: 0'>
              <div class='col-xs-12 col-sm-6 col-md-5 col-lg-4 image-look-my' style='border: 1px solid #b9b9b9; margin-top: 7px; text-align: center;'>
                <img src="<?php echo $thumb; ?>"/>
              </div>
              <div class="my-class-for-product col-xs-12 col-sm-6 col-md-7 col-lg-8">
                  <p style="  font-size: 25px;  font-family: 'ProximaNovaSemiBold';  color: rgb(65, 65, 65);  line-height: 1.2; font-weight: 700;">Назначение:</p>
                <p style="font-size: 16px; font-family: ProximaNova;">Применяется  для снижения пожароопасности всех видов кабелей (силовых, контрольных, связи и др.
                  независимо от величины напряжения и толщины оболочки) в туннелях, коллекторах,
                  производственных помещениях, на эстакадах при наличии козырька, защищающего кабель от
                  прямого воздействия дождя и снега.
                </p>
                <p style="  font-size: 25px;  font-family: 'ProximaNovaSemiBold';  color: rgb(65, 65, 65);  line-height: 1.2; font-weight: 700;">Свойства:</p>
                <ul>
                  <li>
                    <a>
                      Краска обладает хорошей адгезией по отношению к материалам оболочек
                      кабелей (ПВХ, полиэтилену) и не оказывает на них агрессивного
                      воздействия.
                    </a>
                  </li>
                  <li>
                    <a>
                      Экологически безопасная, не содержит органических растворителей - возможно
                      нанесение в туннелях без дополнительной вентиляции.
                    </a>
                  </li>
                  <li>
                    <a>
                      Не растворяет оболочку кабеля.
                    </a>
                  </li>
                  <li>
                    <a>
                      Цвет белый.
                    </a>
                  </li>
                </ul>
                   
              </div>
            </div>


            <ul class="nav nav-tabs hidden-xs hidden-sm hidden-md  col-sm-4 col-md-4 col-lg-4 text-my-button-right right-my marg-button-style" style="margin-top: 30px;">
            <li class="active"><a href="#tab-description" data-toggle="tab" style="  font-size: 15px; font-family: 'ProximaNova';  color: rgb(65, 65, 65);  line-height: 1.2;"><?php echo $tab_description; ?></a></li>
            <li><a href="#tab-specification" data-toggle="tab" style="  font-size: 15px; font-family: 'ProximaNova';  color: rgb(65, 65, 65);  line-height: 1.2;">Техническая спецификация</a></li>
					</ul>
            <div class="hidden-xs col-sm-6 col-md-6 col-lg-4 text-my-button-right right-my marg-button-style">
              <button class="chek-button-my-one" data-toggle="collapse" onclick="openmod()">ПЕРЕЗВОНИТЕ МНЕ
                <img style="margin-bottom: 4px; margin-left: 13px;" src="img/row_right.png">
              </button>
            </div>
            <div class="hidden-xs col-sm-6 col-md-6 col-lg-4 text-my-button-right marg-button-style">
              <button class=" chek-button-my-two" data-toggle="collapse" onclick="f('<?php echo $heading_title;    ?>')">ЗАКАЗАТЬ
                <img style="margin-bottom: 4px; margin-left: 13px;" src="img/row_right.png">
              </button>
            </div>
			
			
			
            <div class="col-xs-12 hidden-sm hidden-md hidden-lg text-my-button-right right-my marg-button-style">
              <button class="chek-button-my-one" data-toggle="collapse" onclick="openmod()">ПЕРЕЗВОНИТЕ МНЕ
                <img style="margin-bottom: 4px; margin-left: 13px;" src="img/row_right.png">
              </button>
            </div>
            <div class="col-xs-12 hidden-sm hidden-md hidden-lg text-my-button-right marg-button-style">
              <button class=" chek-button-my-two" data-toggle="collapse" onclick="f('<?php echo $heading_title;    ?>')">ЗАКАЗАТЬ
                <img style="margin-bottom: 4px; margin-left: 13px;" src="img/row_right.png">
              </button>
            </div>
            <ul class="nav nav-tabs col-xs-12 hidden-lg text-my-button-right right-my marg-button-style" style="margin-top: 30px;">
            <li class="active"><a href="#tab-description" data-toggle="tab" style="  font-size: 15px; font-family: 'ProximaNova';  color: rgb(65, 65, 65);  line-height: 1.2;"><?php echo $tab_description; ?></a></li>
            <li><a href="#tab-specification" data-toggle="tab" style="  font-size: 15px; font-family: 'ProximaNova';  color: rgb(65, 65, 65);  line-height: 1.2;">Техническая спецификация</a></li>
					</ul>
			
              
          </div>
                <?php if ($attribute_groups) { ?>
            <div class='col-xs-12 ' style="margin-bottom: 25px;padding-right: 35px;padding-left: 35px;margin-top: 25px;">
              <?php foreach ($attribute_groups as $attribute_group) { ?>
              <?php if ($attribute_group['attribute_group_id']==10) { ?>
              <div class='col-xs-12 col-sm-6 col-md-6 col-lg-3 all-my-style padd-my-top lef-right-my-padd my-align-text'>
                <div class='col-xs-1 col-sm-1 col-md-1 col-lg-1' style='padding: 0;'>
                  <img src="/catalog/view/theme/default/img/m9.png"/>
                </div>
                <div class='col-xs-6 col-sm-6 col-lg-6 padd-left-my-style text-color-style-my-one'>
                  <span class="title-span-my">Фасовка</span>
                </div>
                <div class="pad-zerro-my col-xs-5 col-sm-5 col-lg-5 center-text-my-style text-color-style-my-two">
                  <span class="body-span-my">- <?php  echo $attribute_group['attribute']['0']['text']; ?></span>
                </div>
              </div>
              <?php } ?>
              <?php if ($attribute_group['attribute_group_id']==9) { ?>
              <div class='col-xs-12 col-sm-6 col-md-6 col-lg-3 all-my-style padd-my-top lef-right-my-padd my-align-text'>
                <div class='col-xs-1 col-sm-1 col-md-1 col-lg-1' style='padding: 0;'>
                  <img src="/catalog/view/theme/default/img/m3.png"/>
                </div>
                <div class='col-xs-6 col-sm-6 col-lg-6 padd-left-my-style text-color-style-my-one'>
                  <span class="title-span-my ">Огнезащитная эффективность</span>
                </div>
                <div class="pad-zerro-my col-xs-5 col-sm-5 col-lg-5 center-text-my-style text-color-style-my-two">
                  <span class="body-span-my">- <?php  echo $attribute_group['attribute']['0']['text']; ?></span>
                </div>
              </div>
              <?php } ?>
              <?php if ($attribute_group['attribute_group_id']==8) { ?>
              <div class='col-xs-12 col-sm-6 col-md-6 col-lg-3 all-my-style padd-my-top lef-right-my-padd my-align-text'>
                <div class='col-xs-1 col-sm-1 col-md-1 col-lg-1' style='padding: 0;'>
                  <img src="/catalog/view/theme/default/img/m2.png"/>
                </div>
                <div class='col-xs-6 col-sm-6 col-lg-6 padd-left-my-style text-color-style-my-one'>
                  <span class="title-span-my">Срок службы</span>
                </div>
                <div class="pad-zerro-my col-xs-5 col-sm-5 col-lg-5 center-text-my-style text-color-style-my-two">
                  <span class="body-span-my">- <?php  echo $attribute_group['attribute']['0']['text']; ?></span>
                </div>
              </div>
              <?php } ?>
              <?php if ($attribute_group['attribute_group_id']==7) {        ?>
              <div class='col-xs-12 col-sm-6 col-md-6 col-lg-3 all-my-style padd-my-top lef-right-my-padd my-align-text'>
                <div class='col-xs-1 col-sm-1 col-md-1 col-lg-1' style='padding: 0;'>
                  <img src="/catalog/view/theme/default/img/m13.png"/>
                </div>
                <div class='col-xs-6 col-sm-6 col-lg-6 padd-left-my-style text-color-style-my-one'>
                  <span class="title-span-my">Область применения</span>
                </div>
                <div class="pad-zerro-my col-xs-5 col-sm-5 col-lg-5  center-text-my-style text-color-style-my-two">
                  <span class="body-span-my">-  <?php echo $attribute_group['attribute']['0']['name'];  ?>/<?php echo $attribute_group['attribute']['1']['name'];  ?></span>
                </div>
              </div>
              <?php } ?>
              <?php } ?>
            </div>
            <?php } ?>
		  <div class="tab-content">
            <div class="tab-pane active" id="tab-description" style="    padding-top: 41%; padding-right: 35px;   padding-left: 35px;"><?php echo $description; ?></div>
           
            <div class="tab-pane" id="tab-specification" style="    padding-top: 41%; padding-right: 10%;   padding-left: 10%;">
          

                <p style="text-align: center; font-family: ProximaNova; font-size: 25px; line-height: 1.2; color: rgb(65, 65, 65); ">Технические данные</p>
    <table class="table"><tbody>


    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Базис<br></td><td>A и C<br></td></tr>

    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Цвета</td><td>По каталогам цветов «Тиккурила Симфония». База А также может использоваться в качестве белой краски. При колеровке в ярко-красные и ярко-желтые цвета поверхность необходимо предварительно загрунтовать грунтовкой.</td></tr>

    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Степень блеска</td><td>Высокоглянцевая</td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Расход</td><td>По металлу 12–14 м2/л, по гладкому (строганному) дереву 10–12 м2/л в зависимости от способа нанесения.</td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Тара</td><td>0,9л; 2,7л; 9л.</td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Разбавитель</td><td>Уайт-спирит 1050 и Растворитель для распыления 1032</td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Соотношения смешивания</td><td>Уайт-спирит 1050 или Растворитель 1032 для распыления. </td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Способ нанесения<br></td><td>Наносится кистью, валиком или распылением. Допускается при
необходимости разбавлять на 5–15% по объему. При нанесении
распылением толщина сухой пленки для одного слоя не должна 
превышать 40 мкм.</td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Время высыхания при +23ºС и относительной влажности 50%</td><td>«От пыли» 3–4 часа. Следующий слой можно наносить через сутки. Внимание! При понижении температуры и увеличении относительной влажности воздуха время высыхания может увеличиваться.</td>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Атмосферостойкость</td><td>Хорошая, в том числе в морской и промышленной среде.</td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Износостойкость</td><td>Хорошая.</td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Стойкость к мытью</td><td>Хорошая при применении обычных моющих средств.</td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Стойкость к химикатам</td><td>Покрытия устойчивы к скипидару, уайт-спириту и денатурату.</td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Сухой остаток</td><td>Около 60%.</td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Плотность</td><td>0,9 - 1,1 кг/л</td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Хранение</td><td>Выдерживает хранение и транспортировку при
отрицательных температурах.</td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Сертификат соответствия</td><td><img src="img/pdf-flat.png" alt=""><a class="list-two-button-subscribe" href="" style="padding-left: 15px;">Декларация о соответствии</a></td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Свидетельство о 
государственной регистрации</td><td><img src="img/pdf-flat.png" alt=""><a class="list-two-button-subscribe" href="" style="padding-left: 15px;">Свидетельство о государственной регистрации</a></td></tr>
    
    <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Сертификат пожарной 
безопасности</td><td><img src="img/pdf-flat.png" alt=""><a class="list-two-button-subscribe" href="" style="padding-left: 15px;">Сертификат пожарной безопасности</a></td></tr>
    
    </tbody></table>
                
                <p style="text-align: center; font-family: ProximaNova; font-size: 25px; line-height: 1.2; color: rgb(65, 65, 65); ">Инструкция по применению</p>
    <table class="table" ><tbody>
        <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Условия при обработке</td><td>Окрашиваемая поверхность должна быть сухой. Температура воздуха
и окрашиваемой поверхности должна быть выше +5°С, а относительная
влажность воздуха - менее 80%. Однако, высыхание в вышеуказанных
предельных условиях идет гораздо медленнее, чем при стандартных 
условиях (+23°С / 50%). Медленнее всего высыхают краски, 
заколерованные в темные оттенки.</td></tr>
         <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Предварительная подготовка</td><td>Неокрашенная поверхность: очистить поверхность от пыли, ржавчины и
других загрязнений и обезжирить ее. Деревянные поверхности, 
древесноволокнистые и древесно-стружечные плиты загрунтовать 
грунтовкой «Otex», а стальные и оцинкованные поверхности – 
противокоррозионной грунтовкой «Rostex Super». Алюминиевые 
поверхности и поверхности, подвергающиеся постоянному воздействию
влаги, загрунтовать «Rostex Super». Алюминиевые поверхности 
рекомендуется до грунтования зашкурить. 
<br/><br/>
Ранее окрашенная поверхность: при окраске поверхностей внутри 
помещений вымыть их моющим средством «Maalipesu», затем тщательно 
промыть водой, дать просохнуть. Для промывки наружных металлических 
поверхностей использовать «Panssaripesu». Отслаивающуюся краску 
удалить скребком, всю поверхность отшлифовать. Пыль от шлифовки 
удалить. При необходимости деревянные поверхности, находящиеся в 
плохом состоянии, грунтовать грунтовкой «Otex». Очищенные до металла 
поверхности загрунтовать грунтовкой «Rostex Super». Впадины и другие 
неровности выправить алкидной шпаклевкой. 
При использовании колерованной краски в ярко-красные и ярко-желтые 
цвета поверхность необходимо предварительно загрунтовать. 
По дереву – грунтовкой «Otex», заколерованной в такой же цвет, 
по металлу - грунтовкой «Rostex Super» (красно-коричневой под 
ярко-красные цвета, светло-серой под ярко-желтые цвета).</td></tr>
         <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Окраска</td><td>Краску перед применением перемешать. При необходимости для 
нанесения кистью разбавить уайт-спиритом, а для распыления – 
растворителем 1032. Наносить краску кистью, валиком или 
распылением 1–2 слоями.</td></tr>
         <tr><td style="width: 250px;  font-size: 16px;  font-family: 'ProximaNova';  color: rgb(111, 111, 111); line-height: 1.2;">Очистка инструментов</td><td>Малярные инструменты промыть уайт-спиритом.</td></tr>
         
    
    </tbody></table>
                
                 <p style="text-align: center; font-family: Myriad Pro; font-size: 25px; line-height: 1.2; color: #414141; ">Охрана труда</p>
                
                 <p style="text-align: left; font-family: Myriad Pro; font-size: 16px; line-height: 1.2; color: #414141; ">Воспламеняющееся вещество. Содержит уайт-спирит. Избегать вдыхания паров растворителей или красочной пыли 
от распыления. Организовать эффективный воздухообмен.</p>
                
                 <div class="col-xs-1 col-sm-1 col-lg-1"></div>
                <div class="col-xs-10 col-sm-10 col-lg-10" style="border-bottom: 1px solid #f24840; margin-bottom: 34px; margin-top: 34px"></div>
                 <p style="text-align: left; font-family: ProximaNova; font-size: 16px; line-height: 1.2;  color: rgb(50, 50, 50); ">Приведённая выше информация основана на лабораторных испытаниях, практическом опыте и представлена 
во всей доступной нам полноте. Качество продукции обеспечивается системой качества компании, 
соответствующей международным стандартам ISO 9001 и ISO 14001. Будучи исключительно производителем, 
мы не имеем возможности контролировать условия использования нашей продукции или те многочисленные 
факторы, которые влияют на её эксплуатацию. Мы не несём ответственности за какой-либо ущерб, связанный 
с применением продукта не по назначению или нарушением требований инструкции по эксплуатации. 
Наша компания также оставляет за собой право вносить изменения в вышеуказанную информацию без 
предварительного уведомления.</p>


            </div>
           
       
          </div>
        </div>
      </div>
     </div>
    <?php echo $column_right; ?></div> <?php echo $content_bottom; ?>
</div>
 <div class="modal fade" id="myModalp"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body" style="text-align: center; ">
                        <div class='my-class-text-right'>
                            <div class='exit-img-class' data-dismiss="modal" aria-label="Close" onclick="$('#myModalp').modal('hide');">
                                <img src="/catalog/view/theme/default/img/exit_button.png">
                            </div>
                        </div>
                        <p class="modal-title">ПЕРЕЗВОНИТЕ МНЕ</p>
                        <div class="two-modal-input same-heght-width">
                            <input type="text" name="namep" placeholder="ФИО">
                        </div>
                        <div class="three-modal-input same-heght-width">
                            <input type="text" name="phonep" placeholder="Телефон">
                        </div>
                        <div>
                            <a class="myButtonPersonal" onclick="return subscribep();">Отправить</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php echo $footer; ?>
