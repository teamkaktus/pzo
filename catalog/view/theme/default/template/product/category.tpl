<?php echo $header; ?>
<div class="container">
  <?php echo $content_top; ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-md-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-8'; ?>
    <?php } else { ?>
    <?php $class = 'col-md-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <?php if ($products) { ?>

      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style=" padding-right: 0px;">
          <div class=" body_info">
            <p class="title-head-body"><?php echo $heading_title; ?></p>
            <p class="mytext-title-body" style="padding-top: 5px">
              <?php echo $description; ?>
            </p>
          </div>

          <div class="col-xs-12 mobile_center">
            <div class='my-mob-title'>
              <p>КАТАЛОГ</p>
            </div>
            <?php foreach ($products as $product) { ?>
            <div class="col-xs-12 for-padding-style-my">
              <div class='col-sm-5 col-md-5 col-lg-4 image-title-cost'>
                <a href='<?php echo $product['href'];?>'>
                    <img src='<?php echo $product['thumb']; ?>'/>
                </a>
              </div>
              <div class="col-sm-7 col-md-7 col-lg-8">
                  <a href='<?php echo $product['href'];?>'>
                      <p class="mytitle_table"><?php echo $product['name'];    ?></p>
                  </a>
                <?php if ($product['attribute_groups']) { ?>
                <div class='my-image-table col-xs-12'>
                  <?php foreach ($product['attribute_groups'] as $attribute_group) { ?>
                  <?php if ($attribute_group['attribute_group_id']==10) { ?>
                  <div class='col-xs-12 col-sm-12 for-image-my no-padd-for-my'>
                    <div class='col-xs-2 col-sm-1 col-md-1 col-lg-1' style='padding: 0;'>
                      <img src="/catalog/view/theme/default/img/m9.png"/>
                    </div>
                    <div class='col-xs-5 col-sm-5 col-md-6 col-lg-6 padd-left-none text-color-style-my-one'>
                      <span>Фасовка</span>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-6 col-lg-5 no-padding text-color-style-my-two">
                      <span class="text-size-my">- <?php  echo $attribute_group['attribute']['0']['text']; ?></span>
                    </div>
                  </div>
                <?php } ?>
                  <?php if ($attribute_group['attribute_group_id']==9) { ?>
                  <div class='col-xs-12 col-sm-12 for-image-my no-padd-for-my'>
                    <div class='col-xs-2 col-sm-1 col-md-1 col-lg-1' style='padding: 0;'>
                      <img src="/catalog/view/theme/default/img/m3.png"/>
                    </div>
                    <div class='col-xs-5 col-sm-5 col-md-6 col-lg-6 padd-left-none text-color-style-my-one'>
                      <span>Огнезащитная ефективность</span>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-6 col-lg-5  no-padding text-color-style-my-two">
                      <span class="text-size-my">- <?php  echo $attribute_group['attribute']['0']['text']; ?></span>
                    </div>
                  </div>
                  <?php } ?>
                  <?php if ($attribute_group['attribute_group_id']==8) { ?>
                  <div class='col-xs-12 col-sm-12 for-image-my no-padd-for-my'>
                    <div class='col-xs-2 col-sm-1 col-md-1 col-lg-1' style='padding: 0;'>
                      <img src="/catalog/view/theme/default/img/m2.png"/>
                    </div>
                    <div class='col-xs-5 col-sm-5 col-md-6 col-lg-6 padd-left-none text-color-style-my-one'>
                      <span>Срок службы</span>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-6 col-lg-5  no-padding text-color-style-my-two">
                      <span class="text-size-my">- <?php  echo $attribute_group['attribute']['0']['text']; ?></span>
                    </div>
                  </div>
                  <?php } ?>
                  <?php if ($attribute_group['attribute_group_id']==7) {        ?>
                  <div class='col-xs-12 col-sm-12 for-image-my no-padd-for-my'>
                    <div class='col-xs-2 col-sm-1 col-md-1 col-lg-1' style='padding: 0;'>
                      <img src="/catalog/view/theme/default/img/m13.png"/>
                    </div>
                    <div class='col-xs-5 col-sm-5 col-md-6 col-lg-6 padd-left-none text-color-style-my-one'>
                      <span>Область применения</span>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-6 col-lg-5  no-padding text-color-style-my-two">
                      <span class="text-size-my">- <?php echo $attribute_group['attribute']['0']['name'];  ?>/<?php echo $attribute_group['attribute']['1']['name'];  ?></span>
                    </div>
                  </div>
                  <?php } ?>
                  <?php } ?>




                </div>
                <?php } ?>
                <div class="my-button-table col-xs-12">
                  <button  class="button_about my-button-about" data-toggle="collapse"  onclick="f('<?php echo $product['name'];    ?>')">ЗАКАЗАТЬ
                    <img style="margin-bottom: 4px; margin-left: 10px;" src="/catalog/view/theme/default/img/row_right.png"/>
                  </button>
                </div>
                  
    
              </div>
            </div>
            <?php } ?>
          </div>

        

      </div>
      <div class="row">
        <div class="col-sm-6 col-xs-5 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 col-xs-7 text-right"><?php echo $results; ?></div>
      </div>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p class="marg-none"><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary myButton-style"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      </div>
    <?php echo $column_right; ?></div><?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>
