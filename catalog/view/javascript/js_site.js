$(document).ready(function () {
    $('#myCarousel').owlCarousel({
            items:1,
            margin:0,
            autoHeight:true,
            nav: false,
            dots: false
    });
    $(".blocks1 ").hover(
        function () {
            $('.img_change1').find('img').attr('src', 'img/homic.jpg')
            $('.img_change1 img').attr('src', 'img/homic_red.jpg')
            $('.blocks1').css('background-color', '#f24941');
            $('.blocks1').css('color', '#fafdfd');
        },
        function () {
            $('.img_change1 img').attr('src', 'img/homic.jpg')
            $('.blocks1').css('background-color', 'white');
            $('.blocks1').css('color', '#f24941');

        });
    $(".blocks2 ").hover(
        function () {
            $('.img_change2').find('img').attr('src', 'img/homic.jpg')
            $('.img_change2 img').attr('src', 'img/homic_red.jpg')
            $('.blocks2').css('background-color', '#f24941');
            $('.blocks2').css('color', '#fafdfd');
        },
        function () {
            $('.img_change2 img').attr('src', 'img/homic.jpg')
            $('.blocks2').css('background-color', 'white');
            $('.blocks2').css('color', '#f24941');

        });
    $(".blocks3 ").hover(
        function () {
            $('.img_change3').find('img').attr('src', 'img/homic.jpg')
            $('.img_change3 img').attr('src', 'img/homic_red.jpg')
            $('.blocks3').css('background-color', '#f24941');
            $('.blocks3').css('color', '#fafdfd');
        },
        function () {
            $('.img_change3 img').attr('src', 'img/homic.jpg')
            $('.blocks3').css('background-color', 'white');
            $('.blocks3').css('color', '#f24941');

        });
    $(".blocks4 ").hover(
        function () {
            $('.img_change4').find('img').attr('src', 'img/homic.jpg')
            $('.img_change4 img').attr('src', 'img/homic_red.jpg')
            $('.blocks4').css('background-color', '#f24941');
            $('.blocks4').css('color', '#fafdfd');
        },
        function () {
            $('.img_change4 img').attr('src', 'img/homic.jpg')
            $('.blocks4').css('background-color', 'white');
            $('.blocks4').css('color', '#f24941');

        });

    $(".blocks5 ").hover(
        function () {
            $('.img_change5').find('img').attr('src', 'img/homic.jpg')
            $('.img_change5 img').attr('src', 'img/homic_red.jpg')
            $('.blocks5').css('background-color', '#f24941');
            $('.blocks5').css('color', '#fafdfd');
        },
        function () {
            $('.img_change5 img').attr('src', 'img/homic.jpg')
            $('.blocks5').css('background-color', 'white');
            $('.blocks5').css('color', '#f24941');

        });

    $(".blocks6 ").hover(
        function () {
            $('.img_change6').find('img').attr('src', 'img/homic.jpg')
            $('.img_change6 img').attr('src', 'img/homic_red.jpg')
            $('.blocks6').css('background-color', '#f24941');
            $('.blocks6').css('color', '#fafdfd');
        },
        function () {
            $('.img_change6 img').attr('src', 'img/homic.jpg')
            $('.blocks6').css('background-color', 'white');
            $('.blocks6').css('color', '#f24941');

        });
    $(document).on('click', '.text_product', function () {
        $('.text_product').css('color', '#f24941')
        $(this).removeClass("text_product")
        $(this).addClass("text_product_new");
    });
    $(document).on('click', '.text_product_new', function () {
        $('.text_product_new').css('color', 'black')
        $(this).removeClass("text_product")
        $(this).addClass("text_product_new");
    });

    $(document).on('click', '.button_about', function () {
        $(".button_about").css('background-color', '#343434')
        $(".button_about").css('color', '#e1e1e1')
        $(".button_about").css('border', '0px')
        $(this).removeClass("button_about")
        $(this).addClass("button_about_nev");

    })
    $(document).on('click', '.button_about_nev', function () {
        $(".button_about_nev").css('background-color', '#f24941')
        $(".button_about_nev").css('color', '#ffffff')
        $(this).removeClass("button_about_nev")
        $(this).addClass("button_about");

    });
    
    $('.drop-down-hover').hover(function () {
        $('.dropdown-menu').css('display','block');
    },
    function () {
        $('.dropdown-menu').css('display','none');
    });

    $(document).on('click', '.search', function () {

        var url = $('base').attr('href') + 'index.php?route=product/search';
        var value = $('footer #search input[name=\'search\']').val();

        if (value) {
            url += '&search=' + encodeURIComponent(value);
        }

        location = url;

    });
//$('#button_send_form').click(function(){
    $('#form_send_footer').submit(function(){
        var form = $('#form_send_footer');
        var url = '/index.php?route=information/contact/send_index_message_form';  
        $.ajax({
            type: 'POST',
            url: url,
            data:  form.serialize(),
            dataType: 'json',
            success:(function(data){
                if(data['status'] == false){
                    $("#myModal").modal("toggle");
                    setTimeout(function(){ $('#myModal').modal('hide'); }, 3000);
                    $('.name-correct-footer').css('border' , '2px solid gray');
                    $('.phone-correct-footer').css('border' , '2px solid gray');                        
                }else{
                    if(!validatName($('.name-correct-footer').val())){
                        $('.name-correct-footer').css('border' , '2px solid red');
                    }else{
                        $('.name-correct-footer').css('border' , '2px solid gray');
                    }
                    if(!validatPhone($('.phone-correct-footer').val())){
                        $('.phone-correct-footer').css('border' , '2px solid red');                        
                    }else{
                        $('.phone-correct-footer').css('border' , '2px solid gray');                        
                    }
                }
            })
        });
        return false;
    });

    /* Search */
    $('#search input[name=\'search\']').parent().find('button').on('click', function() {

    });

    $('#search input[name=\'search\']').on('keydown', function(e) {
        if (e.keyCode == 13) {
            $('footer #search input[name=\'search\']').parent().find('button').trigger('click');
        }
    });


});

function validatName(value){
    var result = /^[a-zA-Zа-яА-Я]{3,}$/u;
    return result.test(value);
}

function validatPhone(value){
    var result = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
    return result.test(value);
}
function f(a) {
   
    document.getElementById('nameprod').value = a;
    $("#myModalz").modal();

}
function openmod() {
   
    
    $("#myModalp").modal();

}

function subscribe(){
			var emailpattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			var email = $('#txtemail').val();
			if(email != "")
			{
				if(!emailpattern.test(email))
				{
					alert("Неправильный Email");
					return false;
				}
				else
				{
					$.ajax({
						url: 'index.php?route=module/newsletters/news',
						type: 'post',
						data:{'email' : $('#txtemail').val(),'fio':$('#txtfio').val(),'phone':$('#txtphone').val()},
						dataType: 'json',
						
									
						success: function(json) {
				
						 $("#myModal").modal("toggle");
                    setTimeout(function(){ $('#myModal').modal('hide'); }, 3000);
                    $('.name-correct-footer').css('border' , '2px solid gray');
                    $('.phone-correct-footer').css('border' , '2px solid gray');
						
						}
						
					});
					return false;
				}
			}
			else
			{
				alert("Email обязательно");
				$(email).focus();
				return false;
			}
			

		}

function subscribep(){

					$.ajax({
						url: '/index.php?route=information/contact/send_index_message_formob',
						type: 'post',
						data:{'fio':$('#namep').val(),'phone':$('#phonep').val()},
						dataType: 'json',		
						success: function(json) {
				
						
						}
						
					});
    $('#myModalp').modal('hide');
                         
						 $("#myModal").modal("toggle");
                    setTimeout(function(){ $('#myModal').modal('hide'); }, 3000);
                    $('.name-correct-footer').css('border' , '2px solid gray');
                    $('.phone-correct-footer').css('border' , '2px solid gray');
					return false;
				
		
			

		}
function subscribez(){
 var b = document.getElementById('nameprod').value;
    console.log(b,$('#namez').val(),$('#orgz').val(),$('#mailz').val(),$('#phonez').val());
					$.ajax({
						url: '/index.php?route=information/contact/send_index_message_formobb',
						type: 'post',
						data:{'prod':b,'name':$('#namez').val(),'org':$('#orgz').val(),'mail':$('#mailz').val(),'phone':$('#phonez').val()},
						dataType: 'json',		
						success: function(json) {
				
						
						}
						
					});
    $('#myModalp').modal('hide');
                         
						 $("#myModal").modal("toggle");
                    setTimeout(function(){ $('#myModal').modal('hide'); }, 3000);
                    $('.name-correct-footer').css('border' , '2px solid gray');
                    $('.phone-correct-footer').css('border' , '2px solid gray');
					return false;
				
		
			

		}
