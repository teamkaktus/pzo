<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		// новини початок

		$this->load->model('catalog/news');

		$data['news'] = array();

		$results = $this->model_catalog_news->getNewsShorts(2);

		foreach ($results as $result) {

			$data['news'][] = array(
				'title'        		=> $result['title'],
				'viewed' 			=> sprintf($this->language->get('text_viewed'), $result['viewed']),
				'description'  		=> utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 250),
				'href'         		=> $this->url->link('information/news/info', 'news_id=' . $result['news_id']),
				'posted'   			=> date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		// новини кінець

		// статті початок

		$this->load->model('catalog/statti');

		$data['statti'] = array();

		$results = $this->model_catalog_statti->getStattiShorts(2);

		foreach ($results as $result) {

			$data['statti'][] = array(
				'title'        		=> $result['title'],
				'viewed' 			=> sprintf($this->language->get('text_viewed'), $result['viewed']),
				'description'  		=> utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 250),
				'href'         		=> $this->url->link('information/statti/info', 'statti_id=' . $result['statti_id']),
				'posted'   			=> date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		// статті кінець

		// інфа про продукцію та її характеристики
		$this->load->model('catalog/information');

		$information_info = $this->model_catalog_information->getInformation(15);

			$data['і_heading_title'] = $information_info['title'];

			$data['і_description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');

		// кінець інфи

		// інфа перша

		$information_info = $this->model_catalog_information->getInformation(16);

		$data['a_heading_title'] = $information_info['title'];

		$data['a_description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');

		// кінець інфи

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}
