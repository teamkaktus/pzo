<?php  
class ControllerModuleFilter_prod extends Controller {
	protected function index() {

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/filter_prod.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/filter_prod.tpl';
		} else {
			$this->template = 'default/template/module/filter_prod.tpl';
		}		
		$this->render();
	}
}
?>