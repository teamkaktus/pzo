<?php
// HTTP
define('HTTP_SERVER', 'http://pzo/');

// HTTPS
define('HTTPS_SERVER', 'http://pzo/');

// DIR
define('DIR_APPLICATION', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/catalog/');
define('DIR_SYSTEM', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/system/');
define('DIR_IMAGE', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/img/');
define('DIR_LANGUAGE', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/catalog/language/');
define('DIR_TEMPLATE', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/catalog/view/theme/');
define('DIR_CONFIG', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/system/config/');
define('DIR_CACHE', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/system/storage/cache/');
define('DIR_DOWNLOAD', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/system/storage/download/');
define('DIR_LOGS', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/system/storage/logs/');
define('DIR_MODIFICATION', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/system/storage/modification/');
define('DIR_UPLOAD', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'pzo');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
