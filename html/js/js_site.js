$(document).ready(function () {
	$('#myCarousel').owlCarousel({
		items:1,
		margin:0,
		autoHeight:true,
		nav: false,
		dots: false
	   });
    $(".blocks1 ").hover(
        function () {
            $('.img_change1').find('img').attr('src', 'img/homic.jpg')
            $('.img_change1 img').attr('src', 'img/homic_red.jpg')
            $('.blocks1').css('background-color', '#f24941');
            $('.blocks1').css('color', '#fafdfd');
        },
        function () {
            $('.img_change1 img').attr('src', 'img/homic.jpg')
            $('.blocks1').css('background-color', 'white');
            $('.blocks1').css('color', '#f24941');

        });
    $(".blocks2 ").hover(
        function () {
            $('.img_change2').find('img').attr('src', 'img/homic.jpg')
            $('.img_change2 img').attr('src', 'img/homic_red.jpg')
            $('.blocks2').css('background-color', '#f24941');
            $('.blocks2').css('color', '#fafdfd');
        },
        function () {
            $('.img_change2 img').attr('src', 'img/homic.jpg')
            $('.blocks2').css('background-color', 'white');
            $('.blocks2').css('color', '#f24941');

        });
    $(".blocks3 ").hover(
        function () {
            $('.img_change3').find('img').attr('src', 'img/homic.jpg')
            $('.img_change3 img').attr('src', 'img/homic_red.jpg')
            $('.blocks3').css('background-color', '#f24941');
            $('.blocks3').css('color', '#fafdfd');
        },
        function () {
            $('.img_change3 img').attr('src', 'img/homic.jpg')
            $('.blocks3').css('background-color', 'white');
            $('.blocks3').css('color', '#f24941');

        });
    $(".blocks4 ").hover(
        function () {
            $('.img_change4').find('img').attr('src', 'img/homic.jpg')
            $('.img_change4 img').attr('src', 'img/homic_red.jpg')
            $('.blocks4').css('background-color', '#f24941');
            $('.blocks4').css('color', '#fafdfd');
        },
        function () {
            $('.img_change4 img').attr('src', 'img/homic.jpg')
            $('.blocks4').css('background-color', 'white');
            $('.blocks4').css('color', '#f24941');

        });

    $(".blocks5 ").hover(
        function () {
            $('.img_change5').find('img').attr('src', 'img/homic.jpg')
            $('.img_change5 img').attr('src', 'img/homic_red.jpg')
            $('.blocks5').css('background-color', '#f24941');
            $('.blocks5').css('color', '#fafdfd');
        },
        function () {
            $('.img_change5 img').attr('src', 'img/homic.jpg')
            $('.blocks5').css('background-color', 'white');
            $('.blocks5').css('color', '#f24941');

        });

    $(".blocks6 ").hover(
        function () {
            $('.img_change6').find('img').attr('src', 'img/homic.jpg')
            $('.img_change6 img').attr('src', 'img/homic_red.jpg')
            $('.blocks6').css('background-color', '#f24941');
            $('.blocks6').css('color', '#fafdfd');
        },
        function () {
            $('.img_change6 img').attr('src', 'img/homic.jpg')
            $('.blocks6').css('background-color', 'white');
            $('.blocks6').css('color', '#f24941');

        });
    $(document).on('click', '.text_product', function () {
        $('.text_product').css('color', '#f24941')
        $(this).removeClass("text_product")
        $(this).addClass("text_product_new");
    });
    $(document).on('click', '.text_product_new', function () {
        $('.text_product_new').css('color', 'black')
        $(this).removeClass("text_product")
        $(this).addClass("text_product_new");
    });

    $(document).on('click', '.button_about', function () {
        $(".button_about").css('background-color', '#343434')
        $(".button_about").css('color', '#e1e1e1')
        $(".button_about").css('border', '0px')
        $(this).removeClass("button_about")
        $(this).addClass("button_about_nev");

    })
    $(document).on('click', '.button_about_nev', function () {
        $(".button_about_nev").css('background-color', '#f24941')
        $(".button_about_nev").css('color', '#ffffff')
        $(this).removeClass("button_about_nev")
        $(this).addClass("button_about");

    })


});