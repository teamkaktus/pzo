<?php
class ModelCatalogStatti extends Model {

	public function addStatti($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "statti SET status = '" . (int)$data['status'] . "', date_added = now()");
	
		$statti_id = $this->db->getLastId();
	
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "statti SET image = '" . $this->db->escape($data['image']) . "' WHERE statti_id = '" . (int)$statti_id . "'");
		}
	
		if (isset($data['date_added'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "statti SET date_added = '" . $this->db->escape($data['date_added']) . "' WHERE statti_id = '" . (int)$statti_id . "'");
		}
	
		foreach ($data['statti_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "statti_description SET statti_id = '" . (int)$statti_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "',  description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "',  meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}
	
		if (isset($data['statti_store'])) {
			foreach ($data['statti_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "statti_to_store SET statti_id = '" . (int)$statti_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
		
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'statti_id=" . (int)$statti_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	
		$this->cache->delete('statti');
	}

	public function editStatti($statti_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "statti SET status = '" . (int)$data['status'] . "' WHERE statti_id = '" . (int)$statti_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "statti SET image = '" . $this->db->escape($data['image']) . "' WHERE statti_id = '" . (int)$statti_id . "'");
		}
		
		if (isset($data['date_added'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "statti SET date_added = '" . $this->db->escape($data['date_added']) . "' WHERE statti_id = '" . (int)$statti_id . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "statti_description WHERE statti_id = '" . (int)$statti_id . "'");
	
		foreach ($data['statti_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "statti_description SET statti_id = '" . (int)$statti_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "',  meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}
	
		$this->db->query("DELETE FROM " . DB_PREFIX . "statti_to_store WHERE statti_id = '" . (int)$statti_id . "'");
	
		if (isset($data['statti_store'])) {		
			foreach ($data['statti_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "statti_to_store SET statti_id = '" . (int)$statti_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
	
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'statti_id=" . (int)$statti_id . "'");
	
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'statti_id=" . (int)$statti_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
	
		$this->cache->delete('statti');
	}

	public function deleteStatti($statti_id) { 
		$this->db->query("DELETE FROM " . DB_PREFIX . "statti WHERE statti_id = '" . (int)$statti_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "statti_description WHERE statti_id = '" . (int)$statti_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "statti_to_store WHERE statti_id = '" . (int)$statti_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'statti_id=" . (int)$statti_id . "'");
	
		$this->cache->delete('statti');
	}

	public function getStattiList($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "statti n LEFT JOIN " . DB_PREFIX . "statti_description nd ON (n.statti_id = nd.statti_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sort_data = array(
				'nd.title',
				'n.date_added'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY nd.title";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$statti_data = $this->cache->get('statti.' . (int)$this->config->get('config_language_id'));

			if (!$statti_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "statti n LEFT JOIN " . DB_PREFIX . "statti_description nd ON (n.statti_id = nd.statti_id) WHERE nd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY nd.title");

				$statti_data = $query->rows;

				$this->cache->set('statti.' . (int)$this->config->get('config_language_id'), $statti_data);
			}

			return $statti_data;
		}
	}

	public function getStattiStory($statti_id) { 
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'statti_id=" . (int)$statti_id . "') AS keyword FROM " . DB_PREFIX . "statti n LEFT JOIN " . DB_PREFIX . "statti_description nd ON (n.statti_id = nd.statti_id) WHERE n.statti_id = '" . (int)$statti_id . "' AND nd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
	
		return $query->row;
	}

	public function getStattiDescriptions($statti_id) { 
		$statti_description_data = array();
	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "statti_description WHERE statti_id = '" . (int)$statti_id . "'");
	
		foreach ($query->rows as $result) {
			$statti_description_data[$result['language_id']] = array(
				'title'            	=> $result['title'],
				'description'      	=> $result['description'],
				'meta_title'        => $result['meta_title'],
				'meta_h1'           => $result['meta_h1'],
				'meta_description' 	=> $result['meta_description'],
				'meta_keyword'		=> $result['meta_keyword'],
			);
		}
	
		return $statti_description_data;
	}

	public function getStattiStores($statti_id) { 
		$stattipage_store_data = array();
	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "statti_to_store WHERE statti_id = '" . (int)$statti_id . "'");
		
		foreach ($query->rows as $result) {
			$stattipage_store_data[] = $result['store_id'];
		}
	
		return $stattipage_store_data;
	}

	public function getTotalStatti() { 

     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "statti");
	
		return $query->row['total'];
	}

	public function setStattiListUrl($url) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE query = 'information/statti'");
		if ($query) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'information/statti'");
			$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` SET `query` = 'information/statti', `keyword` = '" . $this->db->escape($url) . "'");
		}else{
			$this->db->query("INSERT INTO `" . DB_PREFIX . "url_alias` SET `query` = 'information/statti', `keyword` = '" . $this->db->escape($url) . "'");
		}
	}

	public function getStattiListUrl($query) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE query = '" . $this->db->escape($query) . "'");
			if($query->rows){
				return $query->row['keyword'];
			}else{
				return false;
			}
	}
}
?>