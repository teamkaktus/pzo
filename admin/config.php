<?php
// HTTP
define('HTTP_SERVER', 'http://pzo/admin/');
define('HTTP_CATALOG', 'http://pzo/');

// HTTPS
define('HTTPS_SERVER', 'http://pzo/admin/');
define('HTTPS_CATALOG', 'http://pzo/');

// DIR
define('DIR_APPLICATION', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/admin/');
define('DIR_SYSTEM', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/system/');
define('DIR_IMAGE', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/img/');
define('DIR_LANGUAGE', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/admin/language/');
define('DIR_TEMPLATE', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/admin/view/template/');
define('DIR_CONFIG', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/system/config/');
define('DIR_CACHE', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/system/storage/cache/');
define('DIR_DOWNLOAD', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/system/storage/download/');
define('DIR_LOGS', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/system/storage/logs/');
define('DIR_MODIFICATION', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/system/storage/modification/');
define('DIR_UPLOAD', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/system/storage/upload/');
define('DIR_CATALOG', 'E:\MyWork\OpenServer2\OpenServer\domains\pzo/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'pzo');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
