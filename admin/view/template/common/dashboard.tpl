<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1>Инструкции</h1>
      <ul class="breadcrumb">
        <?php /* foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } */ ?>
      </ul>
        
        
  <p>краткие инструкции по редактированию и добавления контента на сайт:</p>            
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Название</th>
        <th>действия</th>
        <th>инструкция</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>НАШИ СТАТЬИ</td>
        <td class="text-center"><a href="/admin/index.php?route=catalog/statti&token=<?php echo $token; ?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst1.pdf" target="_blank">инструкция</a></td>
      
      </tr>
      <tr>
        <td>НОВОСТИ КОМПАНИИ</td>
          <td class="text-center"><a href="/admin/index.php?route=catalog/news&token=<?php echo $token; ?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst2.pdf" target="_blank">инструкция</a></td>
       
      </tr>
      <tr>
        <td>НАШИ КЛИЕНТЫ</td>
          <td class="text-center"><a href="/admin/index.php?route=design/banner/edit&token=<?php echo $token; ?>&banner_id=8" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst3.pdf" target="_blank">инструкция</a></td>
       
      </tr>    
        <tr>
        <td>Название магазина/E-Mail/Телефон</td>
            <td class="text-center"><a href="/admin/index.php?route=setting/setting&token=<?php echo $token; ?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst4.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>О компании</td>
               <td class="text-center"><a href="http://pzo/admin/index.php?route=catalog/information/edit&token=<?php echo $token; ?>&information_id=4" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst5.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Миссия компании</td>
               <td class="text-center"><a href="http://pzo/admin/index.php?route=catalog/information/edit&token=<?php echo $token; ?>&information_id=6" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst6.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Производство</td>
               <td class="text-center"><a href="/admin/index.php?route=catalog/information/edit&token=<?php echo $token; ?>&information_id=3" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst7.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Наша команда</td>
               <td class="text-center"><a href="/admin/index.php?route=catalog/information/edit&token=<?php echo $token; ?>&information_id=5" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst8.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Гарантии качества</td>
               <td class="text-center"><a href="/admin/index.php?route=catalog/information/edit&token=<?php echo $token; ?>&information_id=7" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst9.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Обращение к клиентам</td>
               <td class="text-center"><a href="/admin/index.php?route=catalog/information/edit&token=<?php echo $token; ?>&information_id=8" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst10.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Вакансии</td>
               <td class="text-center"><a href="/admin/index.php?route=catalog/information/edit&token=<?php echo $token; ?>&information_id=9" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst11.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Сертификация текст</td>
               <td class="text-center"><a href="/admin/index.php?route=catalog/information/edit&token=<?php echo $token; ?>&information_id=14" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst12.pdf" target="_blank">инструкция</a></td>
       
      </tr>    
        <tr>
        <td>Сертификати</td>
               <td class="text-center"><a href="/admin/index.php?route=catalog/kachestvo&token=<?php echo $token; ?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst12.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Законодательство</td>
               <td class="text-center"><a href="/admin/index.php?route=catalog/information/edit&token=<?php echo $token; ?>&information_id=10" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst13.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Дилерам</td>
               <td class="text-center"><a href="/admin/index.php?route=catalog/information/edit&token=<?php echo $token; ?>&information_id=11" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst14.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Поставщикам</td>
               <td class="text-center"><a href="/admin/index.php?route=catalog/information/edit&token=<?php echo $token; ?>&information_id=12" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst15.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Отзывы</td>
               <td class="text-center"><a href="/admin/index.php?route=testimonial/testimonial&token=<?php echo $token; ?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst16.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Контакты</td>
               <td class="text-center"><a href="/admin/index.php?route=catalog/information/edit&token=<?php echo $token; ?>&information_id=13" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst17.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Категории</td>
               <td class="text-center"><a href="/admin/index.php?route=catalog/category&token=<?php echo $token; ?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst18.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Товары</td>
               <td class="text-center"><a href="/admin/index.php?route=catalog/product&token=<?php echo $token; ?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst19.pdf" target="_blank">инструкция</a></td>
       
      </tr>
           <tr>
        <td>Слайдер</td>
               <td class="text-center"><a href="/admin/index.php?route=design/banner/edit&token=<?php echo $token; ?>&banner_id=7" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a></td>
        <td><a href="/инструкция/inst20.pdf" target="_blank">инструкция</a></td>
       
      </tr>
      
    </tbody>
  </table>
        
        
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_install) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_install; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php /*foreach ($rows as $row) { ?>
    <div class="row">
      <?php foreach ($row as $dashboard_1) { ?>
      <?php $class = 'col-lg-' . $dashboard_1['width'] . ' col-md-3 col-sm-6'; ?>
      <?php foreach ($row as $dashboard_2) { ?>
      <?php if ($dashboard_2['width'] > 3) { ?>
      <?php $class = 'col-lg-' . $dashboard_1['width'] . ' col-md-12 col-sm-12'; ?>
      <?php } ?>
      <?php } ?>
      <div class="<?php echo $class; ?>"><?php echo $dashboard_1['output']; ?></div>
      <?php } ?>
    </div>
    <?php } */?>
  </div>
</div>
<?php echo $footer; ?>