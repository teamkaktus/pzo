-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 11, 2017 at 10:26 AM
-- Server version: 5.5.50
-- PHP Version: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pzo`
--

-- --------------------------------------------------------

--
-- Table structure for table `oc_kachestvo`
--

CREATE TABLE IF NOT EXISTS `oc_kachestvo` (
  `kachestvo_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  `date_added` date DEFAULT NULL,
  `viewed` int(5) NOT NULL DEFAULT '0',
  `pdf50` varchar(255) NOT NULL,
  `pdf49` varchar(255) NOT NULL,
  `pdf48` varchar(255) NOT NULL,
  `pdf47` varchar(255) NOT NULL,
  `pdf46` varchar(255) NOT NULL,
  `pdf45` varchar(255) NOT NULL,
  `pdf44` varchar(255) NOT NULL,
  `pdf43` varchar(255) NOT NULL,
  `pdf42` varchar(255) NOT NULL,
  `pdf41` varchar(255) NOT NULL,
  `pdf40` varchar(255) NOT NULL,
  `pdf39` varchar(255) NOT NULL,
  `pdf38` varchar(255) NOT NULL,
  `pdf37` varchar(255) NOT NULL,
  `pdf36` varchar(255) NOT NULL,
  `pdf35` varchar(255) NOT NULL,
  `pdf34` varchar(255) NOT NULL,
  `pdf33` varchar(255) NOT NULL,
  `pdf32` varchar(255) NOT NULL,
  `pdf31` varchar(255) NOT NULL,
  `pdf30` varchar(255) NOT NULL,
  `pdf29` varchar(255) NOT NULL,
  `pdf28` varchar(255) NOT NULL,
  `pdf27` varchar(255) NOT NULL,
  `pdf26` varchar(255) NOT NULL,
  `pdf25` varchar(255) NOT NULL,
  `pdf24` varchar(255) NOT NULL,
  `pdf23` varchar(255) NOT NULL,
  `pdf22` varchar(255) NOT NULL,
  `pdf21` varchar(255) NOT NULL,
  `pdf20` varchar(255) NOT NULL,
  `pdf19` varchar(255) NOT NULL,
  `pdf18` varchar(255) NOT NULL,
  `pdf17` varchar(255) NOT NULL,
  `pdf16` varchar(255) NOT NULL,
  `pdf15` varchar(255) NOT NULL,
  `pdf14` varchar(255) NOT NULL,
  `pdf13` varchar(255) NOT NULL,
  `pdf12` varchar(255) NOT NULL,
  `pdf11` varchar(255) NOT NULL,
  `pdf10` varchar(255) NOT NULL,
  `pdf9` varchar(255) NOT NULL,
  `pdf8` varchar(255) NOT NULL,
  `pdf7` varchar(255) NOT NULL,
  `pdf6` varchar(255) NOT NULL,
  `pdf5` varchar(255) NOT NULL,
  `pdf4` varchar(255) NOT NULL,
  `pdf3` varchar(255) NOT NULL,
  `pdf2` varchar(255) NOT NULL,
  `pdf1` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_kachestvo_description`
--

CREATE TABLE IF NOT EXISTS `oc_kachestvo_description` (
  `kachestvo_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_h1` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_kachestvo_to_store`
--

CREATE TABLE IF NOT EXISTS `oc_kachestvo_to_store` (
  `kachestvo_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oc_kachestvo`
--
ALTER TABLE `oc_kachestvo`
  ADD PRIMARY KEY (`kachestvo_id`);

--
-- Indexes for table `oc_kachestvo_description`
--
ALTER TABLE `oc_kachestvo_description`
  ADD PRIMARY KEY (`kachestvo_id`,`language_id`);

--
-- Indexes for table `oc_kachestvo_to_store`
--
ALTER TABLE `oc_kachestvo_to_store`
  ADD PRIMARY KEY (`kachestvo_id`,`store_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oc_kachestvo`
--
ALTER TABLE `oc_kachestvo`
  MODIFY `kachestvo_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
